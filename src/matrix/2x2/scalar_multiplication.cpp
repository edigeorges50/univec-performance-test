#include <random>
#include <fstream>
#include <iostream>
#include <chrono>
#include <filesystem>

#include <Eigen/Dense>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
// Univec includes
#include "univec/Matrix2.hpp"
#include "qtydef/QtyDefinitions.hpp"

#if defined(BLA_VENDOR_APPLE)
#include <Accelerate/Accelerate.h>
#elif defined(BLA_VENDOR_OPEN_BLAS)

#include <cblas.h>
#include <lapacke.h>

#endif

using namespace std;
using namespace dfpe;
using namespace boost::units;
using namespace boost::accumulators;
using namespace si;

using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;
using std::chrono::nanoseconds;

constexpr int M = 2;
constexpr int N = 2;
constexpr int D = M * N;

int main(int argc, char *argv[])
{
    random_device rd;
    mt19937 gen(rd());

    // Create a uniform distribution for generating random double values
    uniform_real_distribution<double> distribution(-1000., 1000);

    const int outer_cycles = 1000; // Number of outer_cycles to run
    const int inner_cycles = 10000; // Number of inner_cycles to run


    // Define accumulators that will be used to calculate meanTime and stdDev for each implementation
    accumulator_set<double, features<tag::mean, tag::variance>> acc_raw;
    accumulator_set<double, features<tag::mean, tag::variance>> acc_sem;
    accumulator_set<double, features<tag::mean, tag::variance>> acc_eig;
    accumulator_set<double, features<tag::mean, tag::variance>> acc_bla;
    accumulator_set<double, features<tag::mean, tag::variance>> acc_uni;

    for (int i = 0; i < outer_cycles; ++i)
    {
        // -------------------------------- Generate working data for the Scalar -----------------------------------
        vector<double> scalar_input(inner_cycles);

        for (int i = 0; i < inner_cycles; ++i)
        {
            scalar_input[i] = distribution(gen);
        }
        // -------------------------------- End of generating working data for the Scalar ----------------------------

        // -------------------------------- Generate working data for "Raw" -----------------------------------
        // Each element of the vector is an array of 4 elements corresponding to a 2x2 matrix values
        vector<array<double, D>> raw_input(inner_cycles);
        vector<array<double, D>> raw_result(inner_cycles);

        for (int i = 0; i < inner_cycles; i++)
        {
            raw_input[i] = {distribution(gen), distribution(gen),
                            distribution(gen), distribution(gen)};
        }

        // -------------------------------- Generate working data for "Semi" -----------------------------------
        vector<array<QtySiLength, D>> sem_input_1(inner_cycles);
        vector<array<QtySiLength, D>> sem_result(inner_cycles);

        for (int i = 0; i < inner_cycles; i++)
        {
            sem_input_1[i] = {raw_input[i][0] * meter, raw_input[i][1] * meter,
                              raw_input[i][2] * meter, raw_input[i][3] * meter};
        }

        // -------------------------------- Generate working data for "Eigen" -----------------------------------
        vector<Eigen::Matrix<double, M, N, Eigen::RowMajor>> eig_input(inner_cycles);
        vector<Eigen::Matrix<double, M, N, Eigen::RowMajor>> eig_result(inner_cycles);

        for (int i = 0; i < inner_cycles; ++i)
        {
            eig_input[i] << raw_input[i][0], raw_input[i][1], raw_input[i][2], raw_input[i][3];
        }

        // -------------------------------- Generate working data for "Blas" ------------------------------------
        vector<array<double, D>> bla_input(inner_cycles);
        vector<array<double, D>> bla_result(inner_cycles);

        for (int i = 0; i < inner_cycles; ++i)
        {
            bla_input[i] = {raw_input[i][0], raw_input[i][1], raw_input[i][2], raw_input[i][3]};
            std::fill(bla_result[i].begin(), bla_result[i].end(), 0.);
        }

        // -------------------------------- Generate working data for "Univec" ------------------------------------
        vector<RMatrix2<QtySiLength>> uni_input(inner_cycles);
        vector<RMatrix2<QtySiLength>> uni_result(inner_cycles);

        for (int i = 0; i < inner_cycles; ++i)
        {
            uni_input[i] = {raw_input[i][0] * meter, raw_input[i][1] * meter,
                            raw_input[i][2] * meter, raw_input[i][3] * meter};
        }

        // -------------------------------- Start of "Raw" performance test -----------------------------------
        auto start_time_raw = high_resolution_clock::now();
        for (int j = 0; j < inner_cycles; ++j)
        {
            #pragma unroll
            for (int k = 0; k < D; ++k)
                raw_result[j][k] = raw_input[j][k] * scalar_input[j];
        }
        auto end_time_raw = high_resolution_clock::now();
        auto duration_raw = duration_cast<nanoseconds>(end_time_raw - start_time_raw).count();
        auto duration_raw_per_cycle = duration_raw / inner_cycles;
        acc_raw(duration_raw_per_cycle);
        // -------------------------------- End of "Raw" performance test -------------------------------------

        // -------------------------------- Start of "Semi" performance test -----------------------------------
        auto start_time_sem = high_resolution_clock::now();
        for (int j = 0; j < inner_cycles; ++j)
        {
            #pragma unroll
            for (int k = 0; k < D; ++k)
                sem_result[j][k] = sem_input_1[j][k] * scalar_input[j];
        }
        auto end_time_sem = high_resolution_clock::now();
        auto duration_sem = duration_cast<nanoseconds>(end_time_sem - start_time_sem).count();
        auto duration_sem_per_cycle = duration_sem / inner_cycles;
        acc_sem(duration_sem_per_cycle);
        // -------------------------------- End of "Semi" performance test -------------------------------------

        // -------------------------------- Start of "Eigen" performance test -----------------------------------
        auto start_time_eig = high_resolution_clock::now();
        for (int j = 0; j < inner_cycles; ++j)
        {
            eig_result[j] = eig_input[j] * scalar_input[j];
        }
        auto end_time_eig = high_resolution_clock::now();
        auto duration_eig = duration_cast<nanoseconds>(end_time_eig - start_time_eig).count();
        auto duration_eig_per_cycle = duration_eig / inner_cycles;
        acc_eig(duration_eig_per_cycle);
        // -------------------------------- End of "Eigen" performance test -------------------------------------

        // -------------------------------- Start of "Blas" performance test -----------------------------------
        auto start_time_bla = high_resolution_clock::now();
        for (int j = 0; j < inner_cycles; ++j)
        {
            cblas_daxpy(D, scalar_input[j], bla_input[j].data(), 1, bla_result[j].data(), 1);
        }
        auto end_time_bla = high_resolution_clock::now();
        auto duration_bla = duration_cast<nanoseconds>(end_time_bla - start_time_bla).count();
        auto duration_bla_per_cycle = duration_bla / inner_cycles;
        acc_bla(duration_bla_per_cycle);
        // -------------------------------- End of "Blas" performance test -------------------------------------

        // -------------------------------- Start of "Univec" performance test --------------------------------
        auto start_time_uni = high_resolution_clock::now();

        for (int j = 0; j < inner_cycles; ++j)
        {
            uni_result[j] = uni_input[j] * scalar_input[j];
        }
        auto end_time_uni = high_resolution_clock::now();

        auto duration_uni = duration_cast<nanoseconds>(end_time_uni - start_time_uni).count();
        auto duration_uni_per_cycle = duration_uni / inner_cycles;
        acc_uni(duration_uni_per_cycle);
        // -------------------------------- End of "Univec" performance test ----------------------------------

        // -------------------------------- Check the results ----------------------------------
        for (unsigned i = 0; i < inner_cycles; i++)
        {
            #pragma unroll
            for (int k = 0; k < D; ++k)
            {
                if (abs(raw_result[i][k] - sem_result[i][k].value()) > 1e-9) throw std::runtime_error("failed validation for Semi");
                if (abs(raw_result[i][k] - eig_result[i](k)) > 1e-9) throw std::runtime_error("failed validation for Eigen");
                if (abs(raw_result[i][k] - bla_result[i][k]) > 1e-9) throw std::runtime_error("failed validation for Blas");
                if (abs(raw_result[i][k] - QtySiLength(uni_result[i].rowMajor(k)) / meter) > 1e-9) throw std::runtime_error("failed validation for Univec");
            }
        }
    }


    double mean_raw = mean(acc_raw);
    double mean_sem = mean(acc_sem);
    double mean_eig = mean(acc_eig);
    double mean_bla = mean(acc_bla);
    double mean_uni = mean(acc_uni);
    double std_dev_raw = sqrt(variance(acc_raw));
    double std_dev_sem = sqrt(variance(acc_sem));
    double std_dev_eig = sqrt(variance(acc_eig));
    double std_dev_bla = sqrt(variance(acc_bla));
    double std_dev_uni = sqrt(variance(acc_uni));

    // Print the performance results
    cout << "Result for running " << inner_cycles << " inner_cycles, with mean and stdev calculated for " << outer_cycles << " outer_cycles." << endl;
    cout << "   Raw: mean_time: " << mean_raw << " ns, stdev: " << std_dev_raw << " ns" << endl;
    cout << "  Semi: mean_time: " << mean_sem << " ns, stdev: " << std_dev_sem << " ns" << endl;
    cout << " Eigen: mean_time: " << mean_eig << " ns, stdev: " << std_dev_eig << " ns" << endl;
    cout << "  Blas: mean_time: " << mean_bla << " ns, stdev: " << std_dev_bla << " ns" << endl;
    cout << "Univec: mean_time: " << mean_uni << " ns, stdev: " << std_dev_uni << " ns" << endl;

    // Save the results to a file
    fstream results_file("result.csv", ios::app);
    if (!results_file.is_open())
    {
        cout << "Unable to open the results file:" << endl;
        return -1;
    }

    results_file.seekg(0, std::ios::end);

    // check if is the first line in the file
    if (results_file.tellg() == 0)
    {
        results_file << "Operation, Outer Iterations, Inner Iterations, Mean Raw, Std Raw, Mean Semi, Std Semi, Mean Eigen, Std Eigen, Mean Blas, Std Blas, Mean Univec, Std Univec" << endl;
    }

    results_file << filesystem::path(argv[0]).filename().string() << ","
                 << outer_cycles << ","
                 << inner_cycles << ","
                 << mean_raw << "," << std_dev_raw << ","
                 << mean_sem << "," << std_dev_sem << ","
                 << mean_eig << "," << std_dev_eig << ","
                 << mean_bla << "," << std_dev_bla << ","
                 << mean_uni << "," << std_dev_uni << endl;

    results_file.close();

    return 0;
}