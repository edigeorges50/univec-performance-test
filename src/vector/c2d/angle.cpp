#include "univec/VectorC2D.hpp"
#include "qtydef/QtyDefinitions.hpp"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <random>
#include <iostream>
#include <fstream>
#include <chrono>
#include <Eigen/Dense>

#if defined(BLA_VENDOR_APPLE)
#include <Accelerate/Accelerate.h>
#elif defined(BLA_VENDOR_OPEN_BLAS)

#include <cblas.h>
#include <lapacke.h>

#endif

#include <array>
#include <filesystem>

using namespace std;
using namespace dfpe;
using namespace boost::units;
using namespace boost::accumulators;

using chrono::high_resolution_clock;
using chrono::duration_cast;
using chrono::duration;
using chrono::nanoseconds;

int main(int argc, char *argv[])
{
    random_device rd;
    mt19937 gen(rd());

    // Define the range of random values
    const double minVal = -1000.0;
    const double maxVal = 1000.0;

    // Create a uniform distribution for generating random double values
    uniform_real_distribution<double> dis(minVal, maxVal);

    // Define accumulators that will be used to calculate meanTime and std_dev for each implementation
    accumulator_set<double, features<tag::mean, tag::variance>> acc_raw;
    accumulator_set<double, features<tag::mean, tag::variance>> acc_sem;
    accumulator_set<double, features<tag::mean, tag::variance>> acc_eig;
    accumulator_set<double, features<tag::mean, tag::variance>> acc_bla;
    accumulator_set<double, features<tag::mean, tag::variance>> acc_uni;

    // Define iterators for the nested loops
    const int main_loop_runs = 1000;
    const int small_loops_runs = 10000;  // iterate small_loops_runs times through implementations and then measure the time, so the time spent to measure the time would be negligible
    constexpr int D = 2;

    // iterate j times through all implementations, so we can have a std_dev in the end
    for (int j = 0; j < main_loop_runs; j++)
    {

        // Initialize vectors to store the values for our vectors / vector components
        // We do this in order to iterate only through the operations of interest, so that the initialization does not interfere with the time measurement

        // Declare arrays for the set of common numerical values for all vectors
        array<double, D> vec1{};
        array<double, D> vec2{};

        // Declare Raw values and results
        vector<array<double, D>> values_raw1(small_loops_runs);
        vector<array<double, D>> values_raw2(small_loops_runs);
        vector<double> norm_raw1(small_loops_runs);
        vector<double> norm_raw2(small_loops_runs);
        vector<double> dot_raw(small_loops_runs);
        vector<double> result_raw(small_loops_runs);

        // Declare Sem values and results
        vector<array<QtySiLength, D>> values_sem1(small_loops_runs);
        vector<array<QtySiLength, D>> values_sem2(small_loops_runs);
        vector<QtySiLength> norm_sem1(small_loops_runs);
        vector<QtySiLength> norm_sem2(small_loops_runs);
        vector<QtySiArea> dot_sem(small_loops_runs);
        vector<QtySiPlaneAngle> result_sem(small_loops_runs);

        // Declare Eigen values and results
        vector<Eigen::Vector2d> values_eig_1(small_loops_runs);
        vector<Eigen::Vector2d> values_eig_2(small_loops_runs);
        vector<double> norm_eig1(small_loops_runs);
        vector<double> norm_eig2(small_loops_runs);
        vector<double> dot_eig(small_loops_runs);
        vector<double> result_eig(small_loops_runs);

        // Declare Blas values
        vector<array<double, D>> values_bla_1(small_loops_runs);
        vector<array<double, D>> values_bla_2(small_loops_runs);
        vector<double> norm_bla1(small_loops_runs);
        vector<double> norm_bla2(small_loops_runs);
        vector<double> dot_bla(small_loops_runs);
        vector<double> result_bla(small_loops_runs);

        // Declare Uni values and results
        vector<VectorC2D<QtySiLength>> values_uni1(small_loops_runs);
        vector<VectorC2D<QtySiLength>> values_uni2(small_loops_runs);
        vector<QtySiPlaneAngle> result_uni(small_loops_runs);

        // Assign values to the already defined variables
        for (int i = 0; i < small_loops_runs; i++)
        {

            // Loop through all dimensions of the vectors
            #pragma unroll
            for (int k = 0; k < D; ++k)
            {

                // Define a set of common numerical values for all vectors
                vec1[k] = dis(gen);
                vec2[k] = dis(gen);

                // Define values for the two Raw vectors
                values_raw1[i][k] = vec1[k];
                values_raw2[i][k] = vec2[k];

                // Define values for the two Semi vectors
                values_sem1[i][k] = vec1[k] * si::meter;
                values_sem2[i][k] = vec2[k] * si::meter;

                // Define values for the two Eigen vectors
                values_eig_1[i][k] = vec1[k];
                values_eig_2[i][k] = vec2[k];

                // Define values for the two Blas vectors
                values_bla_1[i][k] = vec1[k];
                values_bla_2[i][k] = vec2[k];

                // Define values for the two Uni vectors
                values_uni1[i][k] = vec1[k] * si::meter;
                values_uni2[i][k] = vec2[k] * si::meter;

            }
        }


        // Execute Raw function
        auto start_time_raw = high_resolution_clock::now();
        for (unsigned i = 0; i < small_loops_runs; i++)
        {
            norm_raw1[i] = sqrt(values_raw1[i][0] * values_raw1[i][0] + values_raw1[i][1] * values_raw1[i][1]);
            norm_raw2[i] = sqrt(values_raw2[i][0] * values_raw2[i][0] + values_raw2[i][1] * values_raw2[i][1]);
            dot_raw[i] = values_raw1[i][0] * values_raw2[i][0] + values_raw1[i][1] * values_raw2[i][1];
            result_raw[i] = acos(dot_raw[i] / (norm_raw1[i] * norm_raw2[i]));
        }
        auto end_time_raw = high_resolution_clock::now();
        auto duration_raw = duration_cast<nanoseconds>(end_time_raw - start_time_raw).count();
        acc_raw((double) duration_raw / small_loops_runs);


        // Execute Semi function
        auto start_time_sem = high_resolution_clock::now();
        for (unsigned i = 0; i < small_loops_runs; i++)
        {
            norm_sem1[i] = sqrt(values_sem1[i][0] * values_sem1[i][0] + values_sem1[i][1] * values_sem1[i][1]);
            norm_sem2[i] = sqrt(values_sem2[i][0] * values_sem2[i][0] + values_sem2[i][1] * values_sem2[i][1]);
            dot_sem[i] = values_sem1[i][0] * values_sem2[i][0] + values_sem1[i][1] * values_sem2[i][1];
            result_sem[i] = boost::units::acos(dot_sem[i] / (norm_sem1[i] * norm_sem2[i]));
        }
        auto end_time_sem = high_resolution_clock::now();
        auto duration_sem = duration_cast<nanoseconds>(end_time_sem - start_time_sem).count();
        acc_sem((double) duration_sem / small_loops_runs);


        // Execute Eigen function
        auto start_time_eig = high_resolution_clock::now();
        for (unsigned i = 0; i < small_loops_runs; i++)
        {
            dot_eig[i] = values_eig_1[i].dot(values_eig_2[i]);
            norm_eig1[i] = values_eig_1[i].norm();
            norm_eig2[i] = values_eig_2[i].norm();
            result_eig[i] = acos(dot_eig[i] / (norm_eig1[i] * norm_eig2[i]));
        }
        auto end_time_eig = high_resolution_clock::now();
        auto duration_eig = duration_cast<nanoseconds>(end_time_eig - start_time_eig).count();
        acc_eig((double) duration_eig / small_loops_runs);


        // Execute Blas function
        auto start_time_bla = high_resolution_clock::now();
        for (unsigned i = 0; i < small_loops_runs; i++)
        {
            dot_bla[i] = cblas_ddot(D, values_bla_1[i].data(), 1, values_bla_2[i].data(), 1);
            norm_bla1[i] = cblas_dnrm2(D, values_bla_1[i].data(), 1);
            norm_bla2[i] = cblas_dnrm2(D, values_bla_2[i].data(), 1);
            result_bla[i] = acos(dot_bla[i] / (norm_bla1[i] * norm_bla2[i]));
        }
        auto end_time_bla = high_resolution_clock::now();
        auto duration_bla = duration_cast<nanoseconds>(end_time_bla - start_time_bla).count();
        acc_bla((double) duration_bla / small_loops_runs);


        // Execute Univec function
        auto start_time_uni = high_resolution_clock::now();
        for (unsigned i = 0; i < small_loops_runs; i++)
        {
            result_uni[i] = QtySiPlaneAngle(values_uni1[i].angle(values_uni2[i]));
        }
        auto end_time_uni = high_resolution_clock::now();
        auto duration_uni = duration_cast<nanoseconds>(end_time_uni - start_time_uni).count();
        acc_uni((double) duration_uni / small_loops_runs);


        // Check if the results are equals
        for (unsigned i = 0; i < small_loops_runs; i++)
        {
            #pragma unroll
            for (int k = 0; k < D; ++k)
            {
                if (abs(result_raw[i] - result_sem[i].value()) > 1e-5)
                    throw std::runtime_error("failed validation for Semi");
                if (abs(result_raw[i] - result_eig[i]) > 1e-5) throw std::runtime_error("failed validation for Eigen");
                if (abs(result_raw[i] - result_bla[i]) > 1e-5) throw std::runtime_error("failed validation for Blas");
                if (abs(result_raw[i] - result_uni[i].value()) > 1e-5)
                    throw std::runtime_error("failed validation for Univec:" + to_string(result_raw[i]) + " vs " +
                                             to_string(result_uni[i]));
            }
        }

    }


    // Define and initialize both meanTime and std_dev for each implementation
    const double mean_time_raw = mean(acc_raw);
    const double std_dev_raw = sqrt(variance(acc_raw));
    const double mean_time_sem = mean(acc_sem);
    const double std_dev_sem = sqrt(variance(acc_sem));
    const double mean_time_eig = mean(acc_eig);
    const double std_dev_eig = sqrt(variance(acc_eig));
    const double mean_time_bla = mean(acc_bla);
    const double std_dev_bla = sqrt(variance(acc_bla));
    const double mean_time_uni = mean(acc_uni);
    const double std_dev_uni = sqrt(variance(acc_uni));


    string file_name = filesystem::path(argv[0]).filename().string();

    // Print the final results
    cout << "Average time for the following operations: " << endl;

    cout << std::left << file_name << " " << setw(8) << "Raw" << " mean_time: " << setw(12) << std::right
         << mean_time_raw << std::left << " ns, std_dev: " << setw(12) << std::right << std_dev_raw << std::left
         << " ns" << endl;
    cout << std::left << file_name << " " << setw(8) << "Semi" << " mean_time: " << setw(12) << std::right
         << mean_time_sem << std::left << " ns, std_dev: " << setw(12) << std::right << std_dev_sem << std::left
         << " ns" << endl;
    cout << std::left << file_name << " " << setw(8) << "Eigen" << " mean_time: " << setw(12) << std::right
         << mean_time_eig << std::left << " ns, std_dev: " << setw(12) << std::right << std_dev_eig << std::left
         << " ns" << endl;
    cout << std::left << file_name << " " << setw(8) << "Blas" << " mean_time: " << setw(12) << std::right
         << mean_time_bla << std::left << " ns, std_dev: " << setw(12) << std::right << std_dev_bla << std::left
         << " ns" << endl;
    cout << std::left << file_name << " " << setw(8) << "Univec" << " mean_time: " << setw(12) << std::right
         << mean_time_uni << std::left << " ns, std_dev: " << setw(12) << std::right << std_dev_uni << std::left
         << " ns" << endl;

    // Save the results to a file
    fstream results_file("result.csv", ios::app);
    if (!results_file.is_open())
    {
        cout << "Unable to open the results file:" << endl;
        return -1;
    }

    results_file.seekg(0, std::ios::end);

    // check if is the first line in the file
    if (results_file.tellg() == 0)
    {
        results_file << "Operation, Outer Iterations, Inner Iterations, Mean Raw, Std Raw, Mean Semi, Std Semi, Mean Eigen, Std Eigen, Mean Blas, Std Blas, Mean Univec, Std Univec" << endl;
    }

    results_file << filesystem::path(argv[0]).filename().string() << ","
                 << main_loop_runs << ","
                 << small_loops_runs << ","
                 << mean_time_raw << "," << std_dev_raw << ","
                 << mean_time_sem << "," << std_dev_sem << ","
                 << mean_time_eig << "," << std_dev_eig << ","
                 << mean_time_bla << "," << std_dev_bla << ","
                 << mean_time_uni << "," << std_dev_uni << endl;

    results_file.close();

    return 0;
}
