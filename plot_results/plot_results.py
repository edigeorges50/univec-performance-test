#!/usr/bin/env python3

from pathlib import Path
import re
import pandas as pd
import plotly.graph_objs as go
from InquirerPy import inquirer

data_folder = Path.cwd().parent
test_system = ''

files = []
for result_file in data_folder.glob('*.csv'):
    files.append(str(result_file.name))

files = sorted(files, reverse=True)

result_file = inquirer.select(
    message= "Select the result file to plot:",
    choices= files,
    default= None,
).execute()

if result_file:
    result_file_pattern = '^result_([a-z]*)\w*_\d*.csv$'
    # determine the operating system for the tests (macOS or linux)
    match = re.search(result_file_pattern, result_file)
    if match:
        test_system = 'macOS' if match.group(1) == 'darwin' else 'linux'

result_file = data_folder / result_file

# read the result files and create a dataframe
result_df = pd.read_csv(result_file)

# Delete unwanted columns
result_df = result_df.drop(columns=[' Outer Iterations', ' Inner Iterations'], axis=1)

matrix_2x2 = result_df[result_df['Operation'].str.contains('matrix_2x2')]
matrix_2x2 = matrix_2x2.rename(columns={' Mean Blas': ' Mean Blas/Lapack'})
matrix_3x3 = result_df[result_df['Operation'].str.contains('matrix_3x3')]
matrix_3x3 = matrix_3x3.rename(columns={' Mean Blas': ' Mean Blas/Lapack'})
vector_c2d = result_df[result_df['Operation'].str.contains('vector_c2d')]
vector_c3d = result_df[result_df['Operation'].str.contains('vector_c3d')]
vector_c7d = result_df[result_df['Operation'].str.contains('vector_c7d')]

matrix_2x2_labels = matrix_2x2['Operation'].str.replace('matrix_2x2_', '').to_list()
matrix_3x3_labels = matrix_3x3['Operation'].str.replace('matrix_3x3_', '').to_list()
vector_2d_labels = vector_c2d['Operation'].str.replace('vector_c2d_', '').to_list()
vector_3d_labels = vector_c3d['Operation'].str.replace('vector_c3d_', '').to_list()
vector_7d_labels = vector_c7d['Operation'].str.replace('vector_c7d_', '').to_list()
colors = ['#98eecc', '#00c1c5', '#0091c0', '#005eac', '#1a247c']

# Used to specify the figure size when saving the plot
fig_width = 1000
fig_height = 700
fig_scale = 4.0

# -------------------------------- Plot matrix 2x2 -----------------------------------
print(matrix_2x2)
fig_matrix_2x2 = go.Figure(
    layout= go.Layout(
        barmode= "group",
        bargap= 0.22,
        width= 1000,
        height= 600,
        title= dict(
            # text= f"Operations for 2x2 matrices - {test_system}",
            x= 0.5,
            y= 0.95,
            xanchor= 'center',
            yanchor= 'top',
            font= dict(size= 24),
        ),
        xaxis= dict(
            title= 'Operations',
            titlefont= dict(size= 28),
            showgrid= True,
            showline= True,
            linecolor= '#444',
            linewidth= 2,
            mirror= True,
            tickfont= dict(size= 20),
            ticks='inside',
        ),
        yaxis= dict(
            type= 'log',
            title= 'Time [ns]',
            titlefont= dict(size= 28),
            showgrid= True,
            showline= True,
            linecolor= '#444',
            linewidth= 2,
            mirror= True,
            tickfont= dict(size= 16),
            ticks='inside',
        ),
        legend= dict(
            title= 'Library:',
            font = dict(size= 18),
            orientation= 'h',
            yanchor= 'bottom',
            y= 1.02,
            xanchor= 'right',
            x= 1
        ),
    )
)

for index in range(1, len(matrix_2x2.columns), 2):
    col = matrix_2x2.columns[index]
    fig_matrix_2x2.add_bar(
        name= col.replace('Mean ', ''),
        x= matrix_2x2_labels,
        y= matrix_2x2.iloc[:, index],
        # error_y=dict(type='data', array=matrix_2x2.iloc[:, index+1], visible=True),
        marker_color= colors[index//2],
        marker_line_width= 1.5,
        marker_line_color= 'DarkSlateGrey',
        offsetgroup= index,
        width= 0.15,
    )
# fig_matrix_2x2.show()
fig_matrix_2x2.write_image(f"{test_system}/matrix_2x2_{test_system}.png", width= fig_width, height= fig_height, scale= fig_scale)
print(f"Plot 'matrix_2x2_{test_system}.png' was saved successfully in {test_system} directory")

print(matrix_3x3)
# -------------------------------- Plot matrix 3x3 -----------------------------------
fig_matrix_3x3 = go.Figure(
    layout= go.Layout(
        barmode= "group",
        bargap= 0.22,
        width= 1000,
        height= 600,
        title= dict(
            # text= f"Operations for 3x3 matrices - {test_system}",
            x= 0.5,
            y= 0.95,
            xanchor= 'center',
            yanchor= 'top',
            font= dict(size= 24),
        ),
        xaxis= dict(
            title= 'Operations',
            titlefont= dict(size= 28),
            showgrid= True,
            showline= True,
            linecolor= '#444',
            linewidth= 2,
            mirror= True,
            tickfont= dict(size= 20),
            ticks='inside',
        ),
        yaxis= dict(
            type= 'log',
            title= 'Time [ns]',
            titlefont= dict(size= 28),
            showgrid= True,
            showline= True,
            linecolor= '#444',
            linewidth= 2,
            mirror= True,
            tickfont= dict(size= 16),
            ticks='inside',
        ),
        legend= dict(
            title= 'Library:',
            font = dict(size= 18),
            orientation= 'h',
            yanchor= 'bottom',
            y= 1.02,
            xanchor= 'right',
            x= 1
        ),
    )
)

for index in range(1, len(matrix_3x3.columns), 2):
    col = matrix_3x3.columns[index]
    fig_matrix_3x3.add_bar(
        name= col.replace('Mean ', ''),
        x= matrix_3x3_labels,
        y= matrix_3x3.iloc[:, index],
        # error_y=dict(type='data', array=matrix_2x2.iloc[:, index+1], visible=True),
        marker_color= colors[index//2],
        marker_line_width= 1.5,
        marker_line_color= 'DarkSlateGrey',
        offsetgroup= index,
        width= 0.15,
    )
# fig_matrix_3x3.show()
fig_matrix_3x3.write_image(f"{test_system}/matrix_3x3_{test_system}.png", width= fig_width, height= fig_height, scale= fig_scale)
print(f"Plot 'matrix_3x3_{test_system}.png' was saved successfully in {test_system} directory")

print(vector_c2d)
# -------------------------------- Plot vector 2D -----------------------------------
fig_vector_2D = go.Figure(
    layout= go.Layout(
        barmode= "group",
        bargap= 0.22,
        width= 1000,
        height= 600,
        title= dict(
            # text= f"Operations for 2D vectors - {test_system}",
            x= 0.5,
            y= 0.95,
            xanchor= 'center',
            yanchor= 'top',
            font= dict(size= 24),
        ),
        xaxis= dict(
            title= 'Operations',
            titlefont= dict(size= 28),
            showgrid= True,
            showline= True,
            linecolor= '#444',
            linewidth= 2,
            mirror= True,
            tickfont= dict(size= 20),
            ticks='inside',
        ),
        yaxis= dict(
            type= 'log',
            title= 'Time [ns]',
            titlefont= dict(size= 28),
            showgrid= True,
            showline= True,
            linecolor= '#444',
            linewidth= 2,
            mirror= True,
            tickfont= dict(size= 16),
            ticks='inside',
        ),
        legend= dict(
            title= 'Library:',
            font = dict(size= 18),
            orientation= 'h',
            yanchor= 'bottom',
            y= 1.02,
            xanchor= 'right',
            x= 1
        ),
    )
)

for index in range(1, len(vector_c2d.columns), 2):
    col = vector_c2d.columns[index]
    fig_vector_2D.add_bar(
        name= col.replace('Mean ', ''),
        x= vector_2d_labels,
        y= vector_c2d.iloc[:, index],
        # error_y=dict(type='data', array=matrix_2x2.iloc[:, index+1], visible=True),
        marker_color= colors[index//2],
        marker_line_width= 1.5,
        marker_line_color= 'DarkSlateGrey',
        offsetgroup= index,
        width= 0.15,
    )
# fig_vector_2D.show()
fig_vector_2D.write_image(f"{test_system}/vector_2D_{test_system}.png", width= fig_width, height= fig_height, scale= fig_scale)
print(f"Plot 'vector_2D_{test_system}.png' was saved successfully in {test_system} directory")

print(vector_c3d)
# -------------------------------- Plot vector 3D -----------------------------------
fig_vector_3D = go.Figure(
    layout= go.Layout(
        barmode= "group",
        bargap= 0.22,
        width= 1000,
        height= 600,
        title= dict(
            # text= f"Operations for 3D vectors - {test_system}",
            x= 0.5,
            y= 0.95,
            xanchor= 'center',
            yanchor= 'top',
            font= dict(size= 24),
        ),
        xaxis= dict(
            title= 'Operations',
            titlefont= dict(size= 28),
            showgrid= True,
            showline= True,
            linecolor= '#444',
            linewidth= 2,
            mirror= True,
            tickfont= dict(size= 20),
            ticks='inside',
        ),
        yaxis= dict(
            type= 'log',
            title= 'Time [ns]',
            titlefont= dict(size= 28),
            showgrid= True,
            showline= True,
            linecolor= '#444',
            linewidth= 2,
            mirror= True,
            tickfont= dict(size= 16),
            ticks='inside',
        ),
        legend= dict(
            title= 'Library:',
            font = dict(size= 18),
            orientation= 'h',
            yanchor= 'bottom',
            y= 1.02,
            xanchor= 'right',
            x= 1
        ),
    )
)

for index in range(1, len(vector_c3d.columns), 2):
    col = vector_c3d.columns[index]
    fig_vector_3D.add_bar(
        name= col.replace('Mean ', ''),
        x= vector_3d_labels,
        y= vector_c3d.iloc[:, index],
        # error_y=dict(type='data', array=matrix_2x2.iloc[:, index+1], visible=True),
        marker_color= colors[index//2],
        marker_line_width= 1.5,
        marker_line_color= 'DarkSlateGrey',
        offsetgroup= index,
        width= 0.15,
    )
# fig_vector_3D.show()
fig_vector_3D.write_image(f"{test_system}/vector_3D_{test_system}.png", width= fig_width, height= fig_height, scale= fig_scale)
print(f"Plot 'vector_3D_{test_system}.png' was saved successfully in {test_system} directory")

print(vector_c7d)
# -------------------------------- Plot vector 7D -----------------------------------
fig_vector_7D = go.Figure(
    layout= go.Layout(
        barmode= "group",
        bargap= 0.22,
        width= 1000,
        height= 600,
        title= dict(
            # text= f"Operations for 7D vectors - {test_system}",
            x= 0.5,
            y= 0.95,
            xanchor= 'center',
            yanchor= 'top',
            font= dict(size= 24),
        ),
        font= dict(size= 20),
        xaxis= dict(
            title= 'Operations',
            titlefont= dict(size= 28),
            showgrid= True,
            showline= True,
            linecolor= '#444',
            linewidth= 2,
            mirror= True,
            tickfont= dict(size= 20),
            ticks='inside',
        ),
        yaxis= dict(
            type= 'log',
            title= 'Time [ns]',
            titlefont= dict(size= 28),
            showgrid= True,
            showline= True,
            linecolor= '#444',
            linewidth= 2,
            mirror= True,
            tickfont= dict(size= 16),
            ticks='inside',
        ),
        legend= dict(
            title= 'Library:',
            font = dict(size= 18),
            orientation= 'h',
            yanchor= 'bottom',
            y= 1.02,
            xanchor= 'right',
            x= 1
        ),
    )
)

for index in range(1, len(vector_c7d.columns), 2):
    col = vector_c7d.columns[index]
    fig_vector_7D.add_bar(
        name= col.replace('Mean ', ''),
        x= vector_7d_labels,
        y= vector_c7d.iloc[:, index],
        # error_y=dict(type='data', array=matrix_2x2.iloc[:, index+1], visible=True),
        marker_color= colors[index//2],
        marker_line_width= 1.5,
        marker_line_color= 'DarkSlateGrey',
        offsetgroup= index,
        width= 0.15,
    )
# fig_vector_7D.show()
fig_vector_7D.write_image(f"{test_system}/vector_7D_{test_system}.png", width= fig_width, height= fig_height, scale= fig_scale)
print(f"Plot 'vector_7D_{test_system}.png' was saved successfully in {test_system} directory")